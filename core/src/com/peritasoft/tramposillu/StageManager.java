/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.peritasoft.tramposillu.stage.Side;
import com.peritasoft.tramposillu.stage.Stage;

import java.util.HashMap;
import java.util.Map;

public class StageManager extends InputAdapter implements Disposable {
    private final Viewport viewport;
    private final Map<Integer, Stage> heldStages = new HashMap<>();
    private final Vector2 touchCoords = new Vector2();
    private final ShapeRenderer shapes;
    private final Stage[] stages;

    public StageManager(final Assets assets, final Viewport viewport) {
        this.viewport = viewport;
        shapes = new ShapeRenderer();
        stages = new Stage[]{
                new Stage(assets, Side.TOP, Input.Keys.NUM_1, Input.Keys.M),
                new Stage(assets, Side.RIGHT, Input.Keys.NUM_2, Input.Keys.P),
                new Stage(assets, Side.BOTTOM, Input.Keys.NUM_3, Input.Keys.C),
                new Stage(assets, Side.LEFT, Input.Keys.NUM_4, Input.Keys.Q),
        };
    }

    public void act(float delta) {
        for (Stage stage : stages) {
            stage.act(delta);
        }
    }

    @SuppressWarnings("LibGDXFlushInsideLoop")
    public void draw(Batch batch) {
        batch.end();
        for (Stage stage : stages) {
            shapes.begin(ShapeRenderer.ShapeType.Filled);
            Gdx.gl.glClearDepthf(1f);
            Gdx.gl.glClear(GL20.GL_DEPTH_BUFFER_BIT);
            Gdx.gl.glDepthFunc(GL20.GL_LESS);
            Gdx.gl.glEnable(GL20.GL_DEPTH_TEST);
            Gdx.gl.glDepthMask(true);
            Gdx.gl.glColorMask(false, false, false, false);
            stage.clip(shapes);
            //noinspection GDXJavaFlushInsideLoop
            shapes.end();

            batch.setTransformMatrix(stage.getTransformMatrix());
            batch.begin();
            Gdx.gl.glColorMask(true, true, true, true);
            Gdx.gl.glEnable(GL20.GL_DEPTH_TEST);
            Gdx.gl.glDepthFunc(GL20.GL_EQUAL);
            stage.draw(batch);
            //noinspection GDXJavaFlushInsideLoop
            batch.end();
        }
        batch.getTransformMatrix().idt();
        batch.begin();
        Gdx.gl.glDisable(GL20.GL_DEPTH_TEST);
    }

    public boolean allReady() {
        for (final Stage stage : stages) {
            if (!stage.isReady()) return false;
        }
        return true;
    }

    public void start() {
        for (final Stage stage : stages) {
            stage.start();
        }
    }

    public boolean isOver() {
        return countFallen() >= stages.length - 1;
    }

    public boolean allFallen() {
        return countFallen() == stages.length;
    }

    private int countFallen() {
        int count = 0;
        for (final Stage stage : stages) {
            if (stage.isFallen()) count++;
        }
        return count;
    }

    public void finish() {
        for (final Stage stage : stages) {
            stage.finish();
        }
    }

    public void resize() {
        shapes.setProjectionMatrix(viewport.getCamera().combined);
        for (final Stage stage : stages) {
            stage.resize(viewport.getWorldWidth(), viewport.getWorldHeight());
        }
    }

    @Override
    public boolean keyDown(int keycode) {
        final Stage stage = getStageForKey(keycode);
        if (stage == null) return false;
        stage.findCenterPoint(touchCoords);
        viewport.project(touchCoords);
        return touchDown((int) touchCoords.x, (int) touchCoords.y, keycode, Input.Buttons.LEFT);
    }

    @Override
    public boolean keyUp(int keycode) {
        final Stage stage = getStageForKey(keycode);
        if (stage == null) return false;
        stage.findCenterPoint(touchCoords);
        viewport.project(touchCoords);
        return touchUp((int) touchCoords.x, (int) touchCoords.y, keycode, Input.Buttons.LEFT);
    }

    private Stage getStageForKey(int key) {
        for (final Stage stage : stages) {
            if (stage.hasKey(key)) return stage;
        }
        return null;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        touchCoords.set(screenX, screenY);
        viewport.unproject(touchCoords);
        final Stage stage = getStageAtPoint(touchCoords);
        if (stage == null || heldStages.containsValue(stage)) return false;
        heldStages.put(pointer, stage);
        stage.hold();
        return true;
    }

    private Stage getStageAtPoint(Vector2 touchPosition) {
        for (final Stage stage : stages) {
            if (stage.contains(touchPosition)) return stage;
        }
        return null;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        final Stage stage = heldStages.get(pointer);
        if (stage == null) return false;
        releaseStage(pointer, stage);
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        final Stage stage = heldStages.get(pointer);
        if (stage == null) return false;
        touchCoords.set(screenX, screenY);
        viewport.unproject(touchCoords);
        if (stage.contains(touchCoords)) return false;
        releaseStage(pointer, stage);
        return false;
    }

    private void releaseStage(int id, Stage stage) {
        stage.release();
        heldStages.remove(id);
    }

    public void resume() {
        if (heldStages.isEmpty()) return;
        IntArray released = new IntArray(heldStages.size());
        for (int pointer : heldStages.keySet()) {
            if (!Gdx.input.isTouched(pointer)) {
                released.add(pointer);
            }
        }
        for (int i = 0; i < released.size; i++) {
            int pointer = released.get(i);
            releaseStage(pointer, heldStages.get(pointer));
        }
    }

    @Override
    public void dispose() {
        shapes.dispose();
    }
}

/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu.stage;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.tramposillu.Assets;

public class FloatUpState extends State {
    private final TextureRegion pig;
    private final TextureRegion shadow;
    private float x;
    private float offset;
    private float scale = 1f;
    private float actionTime;

    public FloatUpState(Assets assets, Stage stage) {
        super(assets, stage);

        final TextureAtlas atlas = assets.get(Assets.spriteAtlas);
        pig = atlas.findRegion("hanging", 1);
        shadow = atlas.findRegion("shadow", 1);
    }

    @Override
    public void resize(float width, float height) {
        x = width / 2f;
    }

    @Override
    public State act(float deltaTime) {
        actionTime += deltaTime;
        scale -= deltaTime;
        stage.moveBackgroundUp(Math.min(actionTime / 5f, 1f));
        offset = MathUtils.sin(actionTime / 0.5f) * 5f;
        return this;
    }

    @Override
    public void draw(Batch batch) {
        batch.draw(pig, x - pig.getRegionWidth() / 2f + offset, 50f);
        if (scale >= 0f) {
            final float scaledWidth = shadow.getRegionWidth() * scale;
            final float v = (shadow.getRegionWidth() - scaledWidth);
            batch.draw(shadow, x - pig.getRegionWidth() / 2f + offset + v / 2, 50f - v, scaledWidth, shadow.getRegionHeight());
        }
    }

    @Override
    public State release() {
        return new FallDownState(assets, stage, offset);
    }

    @Override
    public State finish() {
        return new ThrowConfettiState(assets, stage, pig, actionTime, offset);
    }
}

/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu.stage;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.peritasoft.tramposillu.Assets;

public class HangState extends State {
    private final Animation<TextureRegion> hangingAnimation;
    private final Animation<TextureRegion> shadowAnimation;
    private float x;
    private float actionTime;

    public HangState(Assets assets, Stage stage) {
        super(assets, stage);

        final TextureAtlas atlas = assets.get(Assets.spriteAtlas);
        hangingAnimation = new Animation<>(0.6f, atlas.findRegions("hanging"), Animation.PlayMode.LOOP);
        shadowAnimation = new Animation<>(0.6f, atlas.findRegions("shadow"), Animation.PlayMode.LOOP);
    }

    @Override
    public void resize(float width, float height) {
        x = (width - Stage.PIG_WIDTH) / 2f;
    }

    @Override
    public State act(float deltaTime) {
        actionTime += deltaTime;
        return this;
    }

    @Override
    public void draw(Batch batch) {
        final TextureRegion hangingFrame = hangingAnimation.getKeyFrame(actionTime, true);
        batch.draw(hangingFrame, x, 50f);
        final TextureRegion shadowFrame = shadowAnimation.getKeyFrame(actionTime, true);
        batch.draw(shadowFrame, x, 50f);
    }

    @Override
    boolean isReady() {
        return true;
    }

    @Override
    public State release() {
        return new RecoverState(assets, stage);
    }

    @Override
    public State start() {
        return new FloatUpState(assets, stage);
    }
}
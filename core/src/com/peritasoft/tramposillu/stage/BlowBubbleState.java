/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu.stage;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.peritasoft.tramposillu.Assets;

public class BlowBubbleState extends State {
    private final Animation<TextureRegion> blowingAnimation;
    private float x;
    private float actionTime;
    private boolean released;

    public BlowBubbleState(Assets assets, Stage stage) {
        super(assets, stage);

        assets.get(Assets.inflateSound).play();
        final TextureAtlas atlas = assets.get(Assets.spriteAtlas);
        blowingAnimation = new Animation<>(0.099f, atlas.findRegions("blowing"));
    }

    @Override
    public void resize(float width, float height) {
        x = (width - Stage.PIG_WIDTH) / 2f;
    }

    @Override
    public State act(float deltaTime) {
        if (blowingAnimation.isAnimationFinished(actionTime)) {
            return released ? new RecoverState(assets, stage) : new HangState(assets, stage);
        }
        actionTime += deltaTime;
        return this;
    }

    @Override
    public void draw(Batch batch) {
        final TextureRegion frame = blowingAnimation.getKeyFrame(actionTime, false);
        batch.draw(frame, x, 50f);
    }

    @Override
    public State hold() {
        released = false;
        return super.hold();
    }

    @Override
    public State release() {
        if (actionTime > blowingAnimation.getFrameDuration() * 8f) {
            released = true;
            return this;
        }
        return new ChewGumState(assets, stage);
    }
}

/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu.stage;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.peritasoft.tramposillu.Assets;

public class FallDownState extends State {
    private final Animation<TextureRegion> fallingAnimation;
    private final float offset;
    private float x;
    private float actionTime;
    private boolean fallen;
    private float fallDelay = 1.2f;

    public FallDownState(Assets assets, Stage stage, float offset) {
        super(assets, stage);
        this.offset = offset;

        assets.get(Assets.plopSound).play();
        final TextureAtlas atlas = assets.get(Assets.spriteAtlas);
        fallingAnimation = new Animation<>(0.2f, atlas.findRegions("falling"));
    }

    @Override
    public void resize(float width, float height) {
        x = width / 2f + offset;
    }

    @Override
    public State act(float deltaTime) {
        if (fallDelay >= 0f) {
            fallDelay -= deltaTime;
            if (fallDelay < 0f) {
                assets.get(Assets.fallSound).play();
            }
        }
        if (fallingAnimation.isAnimationFinished(actionTime)) {
            return new BlackenState(assets, stage);
        }
        actionTime += deltaTime;
        return this;
    }

    @Override
    public void draw(Batch batch) {
        final TextureRegion frame = fallingAnimation.getKeyFrame(actionTime);
        batch.draw(frame, x - frame.getRegionWidth() / 2f, 50f);
    }

    @Override
    public State finish() {
        fallen = true;
        return this;
    }

    @Override
    boolean isFallen() {
        return fallen;
    }
}

/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu.stage;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.peritasoft.tramposillu.Assets;

public class BlackenState extends State {
    private final TextureRegion black;
    private float width;
    private float height;

    public BlackenState(Assets assets, Stage stage) {
        super(assets, stage);

        final TextureAtlas atlas = assets.get(Assets.spriteAtlas);
        black = atlas.findRegion("black");
    }

    @Override
    public void resize(float width, float height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public void draw(Batch batch) {
        final Color color = batch.getColor();
        batch.setColor(color.r, color.g, color.b, 0.3f);
        batch.draw(black, 0f, 0f, width, height);
        batch.setColor(color.r, color.g, color.b, 1f);
    }

    @Override
    boolean isFallen() {
        return true;
    }
}

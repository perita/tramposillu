/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu.stage;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.peritasoft.tramposillu.Assets;

public class RecoverState extends State {
    private final Animation<TextureRegion> recoveringAnimation;
    private float actionTime;
    private float x;
    private boolean held;

    public RecoverState(Assets assets, Stage stage) {
        super(assets, stage);
        final TextureAtlas atlas = assets.get(Assets.spriteAtlas);
        recoveringAnimation = new Animation<>(0.099f, atlas.findRegions("recovering"), Animation.PlayMode.NORMAL);
        assets.get(Assets.plopSound).play();
    }

    @Override
    public void resize(float width, float height) {
        x = width / 2f - 12f;
    }

    @Override
    public State act(float deltaTime) {
        if (recoveringAnimation.isAnimationFinished(actionTime)) {
            return held ? new BlowBubbleState(assets, stage) : new ChewGumState(assets, stage);
        }
        actionTime += deltaTime;
        return this;
    }

    @Override
    public void draw(Batch batch) {
        final TextureRegion frame = recoveringAnimation.getKeyFrame(actionTime);
        batch.draw(frame, x - frame.getRegionWidth() / 2f, 50f);
    }

    @Override
    public State hold() {
        held = true;
        return super.hold();
    }

    @Override
    public State release() {
        held = false;
        return super.release();
    }
}

/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu.stage;

public enum Side {
    TOP(180f, true),
    RIGHT(90f, false),
    BOTTOM(0f, true),
    LEFT(270f, false),
    ;

    public final float degrees;
    public final boolean horizontal;

    Side(float degrees, boolean horizontal) {
        this.degrees = degrees;
        this.horizontal = horizontal;
    }
}

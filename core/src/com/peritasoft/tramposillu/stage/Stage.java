/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu.stage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.*;
import com.badlogic.gdx.utils.Scaling;
import com.peritasoft.tramposillu.Assets;

public class Stage {
    public static final float PIG_WIDTH = 128f;
    private static final float SPACING = 2f;
    private final Polygon area = new Polygon();
    private final Matrix4 transformMatrix = new Matrix4();
    private final Vector2 worldSize = new Vector2();
    private final Side side;
    private final int[] keys;
    private final TextureRegion background;
    private final TextureRegion visibleBackground;
    private State state;

    public Stage(final Assets assets, final Side side, int... key) {
        if (assets == null) throw new IllegalArgumentException("assets cannot be null");
        if (side == null) throw new IllegalArgumentException("side cannot be null");
        this.side = side;
        this.keys = key;
        this.background = assets.get(Assets.spriteAtlas).findRegion("background");
        visibleBackground = new TextureRegion(new TextureRegion(background, 0, background.getRegionHeight() / 2,
                background.getRegionWidth(), background.getRegionHeight() / 2));
        setState(new WalkToCenterState(assets, this));
        resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    public void clip(final ShapeRenderer shapes) {
        float[] vertices = area.getTransformedVertices();
        if (vertices.length != 6) throw new IllegalStateException("area has the wrong number of vertices");
        shapes.triangle(vertices[0], vertices[1], vertices[2], vertices[3], vertices[4], vertices[5]);
    }

    public void moveBackgroundUp(float percent) {
        final float y = MathUtils.lerp(background.getRegionHeight() / 2f, 0f, percent);
        visibleBackground.setRegion(background, 0, MathUtils.round(y), background.getRegionWidth(), background.getRegionHeight() / 2);
    }

    public void resize(float worldWidth, float worldHeight) {
        worldSize.set(worldWidth, worldHeight);
        transformMatrix
                .idt()
                .rotate(Vector3.Z, side.degrees)
        ;
        switch (side) {
            case TOP:
                area.setVertices(new float[]{SPACING, worldSize.y, worldSize.x - SPACING, worldSize.y, worldSize.x / 2f, worldSize.y / 2f + SPACING});
                transformMatrix.translate(-worldWidth, -worldHeight, 0f);
                break;
            case LEFT:
                area.setVertices(new float[]{0, worldSize.y - SPACING, worldSize.x / 2f - SPACING, worldSize.y / 2f, 0f, SPACING});
                transformMatrix.translate(-worldHeight, 0f, 0f);
                break;
            case BOTTOM:
                area.setVertices(new float[]{SPACING, 0f, worldSize.x / 2f, worldSize.y / 2f - SPACING, worldSize.x - SPACING, 0f});
                transformMatrix.translate(0f, 0f, 0f);
                break;
            case RIGHT:
                area.setVertices(new float[]{worldSize.x / 2f + SPACING, worldSize.y / 2f, worldSize.x, worldSize.y - SPACING, worldSize.x, SPACING});
                transformMatrix.translate(0, -worldWidth, 0f);
                break;
        }
        state.resize(getWidth(), getHeight());
    }

    public float getWidth() {
        return side.horizontal ? worldSize.x : worldSize.y;
    }

    public float getHeight() {
        return (side.horizontal ? worldSize.y : worldSize.x) / 2f;
    }

    public void act(float deltaTime) {
        setState(state.act(deltaTime));
    }

    private void setState(State next) {
        if (state == next) return;
        if (next == null) throw new IllegalStateException("next state is null");
        state = next;
        state.resize(getWidth(), getHeight());
    }

    public void draw(final Batch batch) {
        final Vector2 size = Scaling.fill.apply(visibleBackground.getRegionWidth(), visibleBackground.getRegionHeight(), getWidth() / 2, getHeight() / 2);
        batch.draw(visibleBackground, 0f, 0f, size.x * 2, size.y * 2);
        state.draw(batch);
    }

    public Matrix4 getTransformMatrix() {
        return transformMatrix;
    }

    public void hold() {
        setState(state.hold());
    }

    public void release() {
        setState(state.release());
    }

    public void start() {
        setState(state.start());
    }

    public void finish() {
        setState(state.finish());
    }

    public boolean isReady() {
        return state.isReady();
    }

    public boolean isFallen() {
        return state.isFallen();
    }

    public boolean contains(final Vector2 point) {
        return area.contains(point);
    }

    public boolean hasKey(int key) {
        for (int k : keys) {
            if (k == key) return true;
        }
        return false;
    }

    public void findCenterPoint(final Vector2 center) {
        final float[] vertices = area.getTransformedVertices();
        if (vertices.length != 6) throw new IllegalStateException("area has the wrong number of vertices");
        float x = (vertices[0] + vertices[2] + vertices[4]) / 3f;
        float y = (vertices[1] + vertices[3] + vertices[5]) / 3f;
        center.set(x, y);
    }
}
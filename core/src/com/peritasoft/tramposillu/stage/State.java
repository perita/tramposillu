/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu.stage;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.peritasoft.tramposillu.Assets;

public abstract class State {
    protected final Assets assets;
    protected final Stage stage;

    protected State(final Assets assets, final Stage stage) {
        if (assets == null) throw new IllegalArgumentException("assets cannot be null");
        if (stage == null) throw new IllegalArgumentException("stage cannot be null");

        this.assets = assets;
        this.stage = stage;
    }

    public abstract void resize(float width, float height);

    public abstract void draw(Batch batch);

    public State act(float deltaTime) {
        return this;
    }

    public State start() {
        return this;
    }

    public State hold() {
        return this;
    }

    public State release() {
        return this;
    }

    public State finish() {
        return this;
    }

    boolean isReady() {
        return false;
    }

    boolean isFallen() {
        return false;
    }
}

/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu.screen;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.peritasoft.tramposillu.Assets;
import com.peritasoft.tramposillu.Countdown;
import com.peritasoft.tramposillu.StageManager;
import com.peritasoft.tramposillu.Tramposillu;

public class SetScreen extends StagesScreen {
    private final Countdown countdown;
    private final Sound sound;

    public SetScreen(final Tramposillu game, final StageManager stages) {
        super(game, stages);
        countdown = new Countdown(getAssets());
        sound = getAssets().get(Assets.countdownSound);
    }

    @Override
    public void show() {
        super.show();
        sound.play();
    }

    @Override
    public void hide() {
        sound.stop();
        super.hide();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (!stages.allReady()) {
            game.setScreen(new ReadyScreen(game, stages));
        } else if (countdown.isFinished()) {
            game.setScreen(new GoScreen(game, stages));
        } else {
            countdown.act(delta);
        }
    }

    @Override
    public void draw(Batch batch) {
        super.draw(batch);
        countdown.draw(batch, getWorldWidth(), getWorldHeight());
    }
}

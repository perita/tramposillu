/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.peritasoft.tramposillu.Assets;
import com.peritasoft.tramposillu.StageManager;
import com.peritasoft.tramposillu.TapAnimation;
import com.peritasoft.tramposillu.Tramposillu;

public class EndScreen extends StagesScreen {
    private final TapAnimation tapAnimation;
    private final Sound sadSound;
    private final Sound cheerSound;
    private float stateTime;

    public EndScreen(final Tramposillu game, final StageManager stages) {
        super(game, stages);
        tapAnimation = new TapAnimation(getAssets());
        sadSound = getAssets().get(Assets.sadSound);
        cheerSound = getAssets().get(Assets.cheerSound);
    }

    @Override
    public void show() {
        super.show();
        Gdx.input.setInputProcessor(new InputAdapter() {
            @Override
            public boolean touchDown(int screenX, int screenY, int pointer, int button) {
                game.setScreen(new ReadyScreen(game));
                return true;
            }
        });
        stages.finish();
        (stages.allFallen() ? sadSound : cheerSound).play();
    }

    @Override
    protected void act(float delta) {
        super.act(delta);
        stateTime += delta;
    }

    @Override
    protected void draw(Batch batch) {
        super.draw(batch);
        tapAnimation.draw(batch, stateTime, getWorldWidth() / 2f, getWorldHeight() / 2f);
    }

    @Override
    public void hide() {
        super.hide();
        stages.dispose();
        sadSound.stop();
        cheerSound.stop();
    }
}

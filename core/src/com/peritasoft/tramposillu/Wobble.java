/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.MathUtils;

public class Wobble {
    private static final float duration = 0.5f;
    private static final float minDelay = 3f;
    private static final float maxDelay = 10f;
    private float delay;
    private final Sound sound;
    private boolean played;

    public Wobble(final Sound sound) {
        delay = MathUtils.random(minDelay, maxDelay);
        this.sound = sound;
    }

    public float apply(float time) {
        float instant = time - delay;
        if (instant < 0f) return 0f;
        if (!played) {
            sound.play();
            played = true;
        }
        if (instant > duration) {
            delay = time + MathUtils.random(minDelay, maxDelay);
            played = false;
            return 0f;
        }
        return MathUtils.sin(2 * instant / duration * MathUtils.PI2) / 4f;
    }
}

/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class PeritaWinkAnimation {
    private static final float delay = 2f;
    private final Animation<TextureRegion> animation;
    private final TextureRegion name;
    private final Sound winkSound;
    private boolean soundPlayed;
    private float stateTime;

    public PeritaWinkAnimation(final Assets assets) {
        winkSound = assets.get(Assets.winkSound);
        final TextureAtlas atlas = assets.get(Assets.spriteAtlas);
        animation = new Animation<>(0.1f, atlas.findRegions("perita_winking"), Animation.PlayMode.NORMAL);
        name = atlas.findRegion("perita_soft");
    }

    public void act(float delta) {
        stateTime += delta;
        if (stateTime > 2.5f && !soundPlayed) {
            soundPlayed = true;
            winkSound.play();
        }
    }

    public void draw(Batch batch, float worldWidth, float worldHeight) {
        final TextureRegion frame = animation.getKeyFrame(Math.max(0f, stateTime - delay));
        batch.draw(
                frame,
                worldWidth / 2f - frame.getRegionWidth() / 2f,
                worldHeight / 2f + 25f
        );
        float alpha = Math.min(1f, stateTime / 2f);
        batch.setColor(1f, 1f, 1f, alpha);
        batch.draw(
                name,
                worldWidth / 2f - name.getRegionWidth() / 2f,
                worldHeight / 2f - name.getRegionHeight() / 2f
        );
        batch.setColor(1f, 1f, 1f, 1f);
    }

    public boolean isFinished() {
        return animation.isAnimationFinished(stateTime - delay - 0.5f);
    }
}

/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.peritasoft.tramposillu.screen.PeritaSoftScreen;

public class Tramposillu extends Game {
    private static final int SCREEN_WIDTH = 1280;
    private static final int SCREEN_HEIGHT = 720;

    private Assets assets;
    private Viewport viewport;
    private SpriteBatch batch;

    @Override
    public void create() {
        batch = new SpriteBatch();
        viewport = new ExtendViewport(Tramposillu.SCREEN_WIDTH, Tramposillu.SCREEN_HEIGHT);

        assets = new Assets();
        assets.load();
        assets.finishLoading();

        setScreen(new PeritaSoftScreen(this));
    }

    public Assets getAssets() {
        return assets;
    }

    public Viewport getViewport() {
        return viewport;
    }

    public Batch getBatch() {
        return batch;
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height, true);
        batch.setProjectionMatrix(viewport.getCamera().combined);
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
        assets.dispose();
        batch.dispose();
    }
}

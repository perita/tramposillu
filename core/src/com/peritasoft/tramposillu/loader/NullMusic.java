package com.peritasoft.tramposillu.loader;

import com.badlogic.gdx.audio.Music;

public class NullMusic implements Music {
    @Override
    public void play() {
        // Nothing to do.
    }

    @Override
    public void pause() {
        // Nothing to do.
    }

    @Override
    public void stop() {
        // Nothing to do.
    }

    @Override
    public boolean isPlaying() {
        return false;
    }

    @Override
    public boolean isLooping() {
        return false;
    }

    @Override
    public void setLooping(boolean isLooping) {
        // Nothing to do.
    }

    @Override
    public float getVolume() {
        return 0;
    }

    @Override
    public void setVolume(float volume) {
        // Nothing to do.
    }

    @Override
    public void setPan(float pan, float volume) {
        // Nothing to do.
    }

    @Override
    public float getPosition() {
        return 0;
    }

    @Override
    public void setPosition(float position) {
        // Nothing to do.
    }

    @Override
    public void dispose() {
        // Nothing to do.
    }

    @Override
    public void setOnCompletionListener(OnCompletionListener listener) {
        // Nothing to do.
    }
}

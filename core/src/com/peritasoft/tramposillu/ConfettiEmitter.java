/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;

public class ConfettiEmitter {
    private final Confetti[] confetti = new Confetti[250];

    public ConfettiEmitter(final Assets assets, float width, float height) {
        final TextureAtlas atlas = assets.get(Assets.spriteAtlas);
        final TextureRegion[] images = {
                atlas.findRegion("confetti_blue"),
                atlas.findRegion("confetti_red"),
                atlas.findRegion("confetti_yellow"),
        };
        for (int i = 0; i < confetti.length; i++) {
            final TextureRegion image = images[MathUtils.random(images.length - 1)];
            final float x = MathUtils.random(width);
            final float y = MathUtils.random(height / 2) + height;
            confetti[i] = new Confetti(x, y, image);
        }
    }

    public void update(float deltaTime) {
        for (final Confetti c : confetti) {
            c.update(deltaTime);
        }
    }

    public void draw(Batch batch) {
        for (final Confetti c : confetti) {
            c.draw(batch);
        }
    }

    private static class Confetti {
        private final Sprite sprite;
        private final float angle;
        private final float speed;
        private float scale;

        public Confetti(float x, float y, final TextureRegion image) {
            sprite = new Sprite(image);
            sprite.setCenter(x, y);
            sprite.setRotation(MathUtils.random(359f));
            speed = MathUtils.random(180f, 380f);
            angle = MathUtils.random(250, 290);
            scale = MathUtils.random(MathUtils.PI2);
        }

        public void update(float deltaTime) {
            scale += deltaTime;
            sprite.setScale(MathUtils.sin(MathUtils.PI2 * scale * 1.5f), 1f);
            sprite.setRotation(sprite.getRotation() + MathUtils.sin(MathUtils.PI2 * scale));
            sprite.setPosition(
                    sprite.getX() + MathUtils.cosDeg(angle) * speed * deltaTime,
                    sprite.getY() + MathUtils.sinDeg(angle) * speed * deltaTime
            );
        }

        public void draw(Batch batch) {
            sprite.draw(batch);
        }
    }
}
/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;

public class TapAnimation {
    private final TextureRegion hand;
    private final TextureRegion tap;
    private float delay = 2f;


    public TapAnimation(final Assets assets) {
        final TextureAtlas atlas = assets.get(Assets.spriteAtlas);
        hand = atlas.findRegion("hand");
        tap = atlas.findRegion("tap");
    }

    public void draw(Batch batch, float stateTime, float centerX, float centerY) {
        float time = stateTime - delay;
        if (time < 0f) return;
        if (time > 2.2f) {
            delay = stateTime + 5f;
            return;
        }
        final float y = centerY - 300f;
        if (MathUtils.sin(time * 6f) <= 0f) {
            float scale = MathUtils.lerp(0f, 1.15f * 2f, Math.abs(MathUtils.cos(time * 3f)) + 0.2f);
            float pixels = (tap.getRegionWidth() * scale - tap.getRegionWidth()) / 2;
            batch.setColor(1f, 1f, 1f, 1f - scale / (1.15f * 2.5f));
            batch.draw(tap, centerX - pixels, y + 54f - pixels, tap.getRegionWidth() * scale, tap.getRegionHeight() * scale);
            batch.setColor(1f, 1f, 1f, 1f);
        }

        float scale = Math.min(1f, MathUtils.lerp(1.15f, 0.75f, Math.abs(MathUtils.sin(time * 3f))));
        batch.draw(hand, centerX, y, hand.getRegionWidth() * scale, hand.getRegionHeight() * scale);
    }
}

En el Tramposillo cuatro amigos compiten para ver quién es el que aguanta más rato con el dedo puesto encima de la pantalla.

Lo que pasa es que no hay ninguna regla. Les puedes hacer cosquillas a los otros. Un empujoncito. Quizás una zancadilla. ¿Y si les lames la mano?

¡Todo vale!

Pero no dejes que sean ellos los que te levanten el dedo de la pantalla, ni siquiera un segundo, o tu cerdito va a caerse a plomo hacia el suelo. No, hombre, no. ¡Que esto no está bien!

Eso sí: ¡no os paséis! Nosotros no nos hacemos responsables de pantallas o amistades rotas.
